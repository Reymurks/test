@servers(['web' => 'reza@adsd2019.clow.nl'])

@setup
$gitUrl = 'git@gitlab.com:Reymurks/test.git';
$branch = (!empty($branch)) ? $branch : 'master';
$basePath ='/home/reza/public_html/scrumapp';
@endsetup

@story('deploy:cold')
git:init
lib:install
storage:link
migrate
@endstory

@story('deploy')
git:update
lib:install
storage:link
migrate
@endstory


@task('git:init')
cd {{ $basePath }}
git init
git remote add origin {{ $gitUrl }}
git pull origin {{ $branch }}
@endtask

@task('git:update')
cd {{ $basePath }}
git pull origin {{ $branch }}
@endtask

@task('lib:install')
cd {{ $basePath }}
composer install
npm install
@endtask

@task('storage:link')
cd {{ $basePath }}
php artisan storage:link
@endtask

@task('migrate')
cd {{ $basePath }}
php artisan migrate
chmod -R 777 storage
@endtask
