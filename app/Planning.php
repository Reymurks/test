<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Planning extends Model
{
    public function backlog_items()
    {
        return $this->hasMany(BacklogItem::class);
    }
}
