<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BacklogItem extends Model
{
    public function planning()
    {
        return $this->belongsTo(Planning::class);
    }
    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
