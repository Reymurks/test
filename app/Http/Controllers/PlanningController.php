<?php

namespace App\Http\Controllers;

use App\Planning;
use Illuminate\Http\Request;


class PlanningController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $planning = Planning::all();
        return view('planning', compact('planning'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $planning = new Planning();

        $planning->id = $request->id;
        $planning->description = $request->description;
        $planning->save();

        return redirect()->route('planning');
    }

        public function deleteplanning(Request $request)
    {

        $planning = Planning::findOrFail($request->id);
        $planning->delete();

        return redirect('/planning');
    }
}
