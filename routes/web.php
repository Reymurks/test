<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/home', 'HomeController@index')->name('home');
Route::post('/save', 'HomeController@save')->name('backlogItemSave'); //Les 2
Route::delete('/removebli', 'homeController@removebli');
Route::get('/planning','PlanningController@index')->name('planning');
Route::post('/planning','PlanningController@store')->name('planningstore');
Route::delete('/deleteplanning','PlanningController@deleteplanning');

Auth::routes();

Route::resource('project', 'ProjectController');